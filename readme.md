# Ferdowsi University Git Exercise

Final Git Graph:
```txt
$ git log --oneline --decorate --graph --color
*   49b5f17 (HEAD -> feature2, develop) Merge branch 'hotfix1' into develop
|\  
| * c66f134 (tag: v1.01) fix feature 1 bug Y
* | 407893f fix feature 1 bug X
* |   21cb75d Merge branch 'feature1' into develop
|\ \  
| * | a2bb1de add feature 1
| |/  
| * 764d2f3 (tag: v1.00) fix feature 1 bug X
|/  
*   3e1e8ea Merge branch 'feature1' into develop
|\  
| * ddf11de add feature 1
| * 4210ff4 add feature 1 wip
|/  
* 2427428 add fileA.txt
* eb8106f (main) add README.md
```
